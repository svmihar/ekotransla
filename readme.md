# forecasting 3 atribut pelindo
[PRESENTASI LIHAT DISINI](https://docs.google.com/presentation/d/19ANVsxduBOy8fpNd9L5rZOWQZJQZBfuOtQNQF89Kok0/edit?usp=sharing)

[VIDEO RUNNING BISA LIHAT DISINI](https://youtu.be/r-p1DorQ9Hk)

## metode
1. acquire data dari pelindo
2. save data ke dalam format csv dengan kolom `['tahun', 'unit']`
    - normalisasi data dengan mengubahnya ke dalam [skala logarithmic](https://en.wikipedia.org/wiki/Logarithmic_scale)
3. masukkan ke dalam prophet
4. kembalikan data ke dalam skala normal, dengan di exponen.

## penjelasan metode
### prophet
ARIMA dengan sedikit perubahan:
- growth forecasting: piecewise logistic growth model
- seasonality: fourier series dengan model efek periodik (Harvey & Shephard 1993)
    - fitting dengan yearly seasonality, dianggap 1 tahun 365.25 hari
- model fitting: estimasi posteriori L-FBGS
```
model {
// Priors
k ∼ normal(0, 5);
m ∼ normal(0, 5);
epsilon ∼ normal(0, 0.5);
delta ∼ double_exponential(0, tau);
beta ∼ normal(0, sigma);
// Logistic likelihood
y ∼ normal(C ./ (1 + exp(-(k + A * delta) .* (t - (m + A * gamma)))) +
X * beta, epsilon);
// Linear likelihood
y ∼ normal((k + A * delta) .* t + (m + A * gamma) + X * beta, sigma);}
```
probabilitas log akan di iterasi 10,000 kali (diasumsikan sudah konvergen)
### akurasi / error rate
dihitung dengan mape untuk mengukur sebarapa banyak perubahan dari inisial probabilitas

## hasil akhir
### kapal
MAPE: 0.000202619
![](./predictions/kapal.png)

## petikemas
MAPE: 0.000108058
![](./predictions/petikemas.png)

## barang
MAPE:0.000164055
![](./predictions/barang.png)

## referensi
- [prophet](./prophet.pdf)

## kesimpulan
model sudah fitting dengan data yang tersedia dilihat dari MAPE, dari hasil ini diprediksi seluruh data akan naik pada 2020, dan akan turun pada 2021.

---

# reproducing code
0. clone repo ini

## cara 1 dengan poetry
1. pastikan poetry (python env manager) diinstall dengan baik dan benar
guide bisa liat [disini](https://python-poetry.org/docs/)
2. run `poetry update` pada direktori repo (pastikan sudah di dalam direktori repo, ditandai dengan adanya file `pyproject.toml`)
3. masuk ke dalam shell poetry yang sudah dibuat `poetry shell` (ditandai dengan nama enviroment di depan terminal)
4. run `python train.py`

## cara 2 dengan pip
1. run `pip install -r requirements.txt`
2. run `python train.py`
