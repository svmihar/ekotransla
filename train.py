import pandas as pd
import time
from pathlib import Path
import numpy as np
from fbprophet import Prophet
from fbprophet.plot import plot_components

FOLDER = Path('./data')
data= {}
for csv in FOLDER.iterdir():
    csv_nya = pd.read_csv(csv)
    csv_nya['tahun'] = pd.to_datetime(csv_nya['tahun'])
    data[str(csv).split('.')[0].split('/')[-1]]= csv_nya

# satu sample yaitu kapal
for k,v in data.items():
    v.columns = ['ds','y']
    v['y_orig'] = v['y']
    v['y_log'] = v['y']
    v['y'] = v['y_orig']
    # log scale
    v['y'] = v['y'].apply(np.log)
    prophet_basic = Prophet(seasonality_mode='multiplicative')
    prophet_basic.fit(v)
    future = prophet_basic.make_future_dataframe(periods=2, freq='y')
    forecast = prophet_basic.predict(future)
    # go to original scale
    forecast_data_orig = forecast
    # forecast_data_orig['yhat'] = v['y_orig']
    forecast_data_orig['yhat'] = np.exp(forecast_data_orig['yhat'])
    forecast_data_orig['yhat_lower'] = np.exp(forecast_data_orig['yhat_lower'])
    forecast_data_orig['yhat_upper'] = np.exp(forecast_data_orig['yhat_upper'])
    # data points to original exp log scale
    breakpoint()
    forecast_data_orig[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].to_csv(f'{k}.csv', index=False)
    fig =prophet_basic.plot(forecast_data_orig)
    # breakpoint()
    fig.savefig(f'{k}.png')
